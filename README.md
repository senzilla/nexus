nexus.css - brings back the first browser
=========================================

A classless CSS framework that makes content look like it would have in
the very first web browser – the Nexus browser.

Nexus.css brings back the simple and beautiful feeling of the original
World Wide Web. The styling brings across the content with great
typography and a clear hierarchy of elements for better readability.

See: [https://worldwideweb.cern.ch/browser/](https://worldwideweb.cern.ch/browser/)
