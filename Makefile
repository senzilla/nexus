# SPDX-License-Identifier: ISC

.include <sz.own.mk>

.MAIN: all

.PHONY: all
all: src/nexus.css src/nexus.min.css

src/nexus.min.css: src/nexus.css
	redirfd -w 1 $@ sz-mincss $>

.include <sz.release.mk>
